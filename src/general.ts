const uniquePredicate = <T>(value: T, index: number, self: T[]) => self.indexOf(value) === index;

export const unique = <T>(array: T[]) => array.filter(uniquePredicate);
