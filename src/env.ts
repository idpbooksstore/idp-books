import dotenv from 'dotenv';
import fs from 'fs';

dotenv.config();

export const env = {
  server: {
    port: +(process.env.PORT ?? 8080),
  },
  mongo: {
    host: process.env.MONGO_HOST ?? 'localhost',
    database: process.env.MONGO_DATABASE ?? 'books',
    username: process.env.MONGO_USERNAME ?? 'root',
    password: process.env.MONGO_PASSWORD ?? 'example',
    username_file: process.env.MONGO_USERNAME_FILE,
    password_file: process.env.MONGO_PASSWORD_FILE,
  },
  nodeEnv: process.env.NODE_ENV ?? 'development',
};

if (env.mongo.username_file) {
  env.mongo.username = fs.readFileSync(env.mongo.username_file, 'utf8');
}
if (env.mongo.password_file) {
  env.mongo.password = fs.readFileSync(env.mongo.password_file, 'utf8');
}
