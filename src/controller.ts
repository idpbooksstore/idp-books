import { getBooksService, getFiltersService } from './service';
import Router, { Request, Response } from 'express';
import { validateFilters } from './validate';

export const router = Router();

export const getBooksRoute = async (req: Request, res: Response) => {
  if (!validateFilters(req.query)) return res.status(400).send({ message: validateFilters.errors, books: [] });
  try {
    const id = req.query.id as string[];
    const genre = req.query.genre as string[];
    const author = req.query.author as string[];
    const { books } = await getBooksService(id, genre, author);

    res.status(200).send({ message: 'Success', books });
  } catch (error) {
    return res.status(500).send({ message: error, books: [] });
  }
};

export const getFiltersRoute = async (req: Request, res: Response) => {
  try {
    const filters = await getFiltersService();

    res.status(200).send({ message: 'Success', filters });
  } catch (error) {
    return res.status(500).send({ message: error, filters: {} });
  }
};

router.get('/', getBooksRoute);
router.get('/filters', getFiltersRoute);
