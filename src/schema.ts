import { Document, Schema, Model, model } from 'mongoose';
import { BookData } from './types';

export interface BookDocument extends Document, BookData {
  _id: string;
}

export interface BookModel extends Model<BookDocument> {}

const bookSchema = new Schema<BookDocument, BookModel>({
  title: { type: String, required: true },
  author: { type: String, required: true },
  genre: { type: String, required: true },
  reviews: [{ score: { type: Number, required: true }, message: { type: String, required: true } }],
  price: { type: Number, required: true },
});

export const Book = model<BookDocument, BookModel>('Book', bookSchema);
