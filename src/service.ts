import { unique } from './general';
import { findBooks } from './model';

export const getBooksService = async (id: string[], genre: string[], author: string[]) => findBooks(id, genre, author);

interface FindFilters {
  author: string[];
  genre: string[];
}

export const getFiltersService = async (): Promise<FindFilters> => {
  const { books } = await findBooks();
  const author = unique(books.map((book) => book.author));
  const genre = unique(books.map((book) => book.genre));

  return { author, genre };
};
