export interface BookData {
  _id: string;
  title: string;
  author: string;
  genre: string;
  reviews: ReviewData[];
  price: number;
}

export interface ReviewData {
  score: number;
  message: string;
}
