import { Book } from './schema';
import { BookData } from './types';

interface FindBooks {
  books: BookData[];
}

export const findBooks = async (id: string[] = [], genre: string[] = [], author: string[] = []): Promise<FindBooks> => {
  const books = await Book.find({
    _id: id.length === 0 ? { $exists: true } : { $in: id },
    genre: genre.length === 0 ? { $exists: true } : { $in: genre },
    author: author.length === 0 ? { $exists: true } : { $in: author },
  }).exec();

  return { books };
};
