import Ajv from 'ajv';

const ajv = new Ajv();

const filtersSchema = {
  type: 'object',
  properties: {
    id: { type: 'array', items: { type: 'string' } },
    genre: { type: 'array', items: { type: 'string' } },
    author: { type: 'array', items: { type: 'string' } },
  },
  additionalProperties: false,
};

export const validateFilters = ajv.compile(filtersSchema);
