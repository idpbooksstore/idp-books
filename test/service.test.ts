jest.mock('../src/model');

import { getFiltersService } from '../src/service';
import { findBooks } from '../src/model';
import { mockFunction } from './util';

describe('Books Service', () => {
  describe('Get Books', () => {});
  describe('Get Filters', () => {
    test('When the model returns an empty array of books, then the result should be empty arrays of authors and genres', async () => {
      const mockedFn = mockFunction(findBooks);
      mockedFn.mockImplementation(async () => {
        return {
          books: [],
        };
      });

      const result = await getFiltersService();

      expect(result).toEqual({ author: [], genre: [] });
    });
    test('When model returns an array of books, then the result should be unique arrays of authors and genres', async () => {
      const mockedFn = mockFunction(findBooks);
      mockedFn.mockImplementation(async () => {
        return {
          books: [
            { _id: 'foo', author: 'A', genre: 'Z', title: 'X', reviews: [], price: 100 },
            { _id: 'foo', author: 'B', genre: 'Z', title: 'X', reviews: [], price: 100 },
            { _id: 'foo', author: 'B', genre: 'Y', title: 'X', reviews: [], price: 100 },
          ],
        };
      });

      const result = await getFiltersService();

      expect(result).toEqual({ author: ['A', 'B'], genre: ['Z', 'Y'] });
    });
  });
});
