jest.mock('../src/service');

import { getBooksRoute, getFiltersRoute } from '../src/controller';
import { getBooksService } from '../src/service';
import httpMocks from 'node-mocks-http';
import { mockFunction } from './util';

describe('Books Controller', () => {
  describe('Get Books', () => {
    test('When the query contains a string parameter, then the api should respond with status 400', async () => {
      const mockedGetBooksService = mockFunction(getBooksService);
      mockedGetBooksService.mockImplementation(async (id: string[], genre: string[], author: string[]) => {
        return { books: [] };
      });
      const request = httpMocks.createRequest({
        method: 'GET',
        url: '/api/books',
        query: {
          id: '123',
          author: ['asd'],
        },
      });
      const response = httpMocks.createResponse();

      await getBooksRoute(request, response);

      expect(response.statusCode).toEqual(400);
    });
    test('When the query contains a number parameter, then the api should respond with status 400', async () => {
      const mockedGetBooksService = mockFunction(getBooksService);
      mockedGetBooksService.mockImplementation(async (id: string[], genre: string[], author: string[]) => {
        return { books: [] };
      });
      const request = httpMocks.createRequest({
        method: 'GET',
        url: '/api/books',
        query: {
          id: 123,
          author: ['asd'],
        },
      });
      const response = httpMocks.createResponse();

      await getBooksRoute(request, response);

      expect(response.statusCode).toEqual(400);
    });
    test('When the query contains an invalid string array, then the api should respond with status 400', async () => {
      const mockedGetBooksService = mockFunction(getBooksService);
      mockedGetBooksService.mockImplementation(async (id: string[], genre: string[], author: string[]) => {
        return { books: [] };
      });
      const request = httpMocks.createRequest({
        method: 'GET',
        url: '/api/books',
        query: {
          id: ['123'],
          author: ['asd', 123],
        },
      });
      const response = httpMocks.createResponse();

      await getBooksRoute(request, response);

      expect(response.statusCode).toEqual(400);
    });
    test('When the query contains an invalid parameter name, then the api should respond with status 400', async () => {
      const mockedGetBooksService = mockFunction(getBooksService);
      mockedGetBooksService.mockImplementation(async (id: string[], genre: string[], author: string[]) => {
        return { books: [] };
      });
      const request = httpMocks.createRequest({
        method: 'GET',
        url: '/api/books',
        query: {
          id: ['123'],
          author: ['asd', 123],
          invalid: 'asd'
        },
      });
      const response = httpMocks.createResponse();

      await getBooksRoute(request, response);

      expect(response.statusCode).toEqual(400);
    });
    test('When the query contains only string arrays, then the api should respond with status 200', async () => {
      const mockedGetBooksService = mockFunction(getBooksService);
      mockedGetBooksService.mockImplementation(async (id: string[], genre: string[], author: string[]) => {
        return { books: [] };
      });
      const request = httpMocks.createRequest({
        method: 'GET',
        url: '/api/books',
        query: {
          id: ['123'],
          author: ['asd', 'asd'],
        },
      });
      const response = httpMocks.createResponse();

      await getBooksRoute(request, response);

      expect(response.statusCode).toEqual(200);
    });
  });
  describe('Get Filters', () => {
    test('When the route is called, then the api should respond with status 200', async () => {
      const request = httpMocks.createRequest({
        method: 'GET',
        url: '/api/filters',
      });
      const response = httpMocks.createResponse();

      await getFiltersRoute(request, response);

      expect(response.statusCode).toEqual(200);
    });
  })
});
